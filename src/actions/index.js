export const addTask = (text) => {
	return {
		type: 'ADD_TASK',
		payload: {
			id: new Date().getTime(),
			text: text,
			completed: false
		}
	};
};
export const deleteTask = (id) => {
	return {
		type: 'DELETE_TASK',
		payload: id
	};
};

export const handleTask = (id) => {
	return {
		type: 'HANDLE_TASK',
		payload: id
	};
};

export const showAll = () => {
	return {
		type: 'SHOW_ALL',
		payload: 'SHOW_ALL'
	};
};

export const showCompleted = () => {
	return {
		type: 'SHOW_COMPLETED',
		payload: 'SHOW_COMPLETED'
	};
};
export const showRemaining = () => {
	return {
		type: 'SHOW_REMAINING',
		payload: 'SHOW_REMAINING'
	};
};

export const setVisibleOption = (option) => {
	return {
		type: option,
		payload: option
	};
};
