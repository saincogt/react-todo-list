/* eslint-disable react/prop-types */
import React from 'react';
import { handleTask } from '../actions';
import { connect } from 'react-redux';
import { ListItem, ListItemIcon, ListItemText, Checkbox, Divider } from '@material-ui/core';

const renderTodoItem = (completed) => {
	if (completed) {
		return { textDecoration: 'line-through', padding: 0 };
	}
	return { padding: 0 };
};

const TodoItem = ({ id, text, completed, dispatch }) => {
	return (
		<React.Fragment>
			<Divider variant='fullWidth' />
			<ListItem
				key={id}
				onClick={() => dispatch(handleTask(id))}
				style={renderTodoItem(completed)}
			>
				<ListItemIcon>
					<Checkbox checked={completed} />
				</ListItemIcon>
				<ListItemText primary={text}/>
			</ListItem>
		</React.Fragment>
	);
};

const mapStateToProps = (state) => {
	return {
		tasks: state.tasks
	};
};

export default connect(mapStateToProps)(TodoItem);
