import React from 'react';
import NewTask from '../components/NewTask';
import TodoListContainer from '../containers/TodoListContainer';
import Footer from './Footer';
import Logo from '../assets/logo.svg';
import { CssBaseline, Container, AppBar, Paper, Toolbar } from '@material-ui/core';
import './App.css';

const App = () => {
	return (
		<React.Fragment>
			<AppBar position='sticky' className='app-bar'>
				<img src={Logo} alt='React To-do List' className='logo' />
				<Toolbar>
					React Todo List
				</Toolbar>
			</AppBar>
			<CssBaseline />
			<Container maxWidth='sm'>
				<Paper style={{ marginTop: 16, marginBottom: 16, padding: 16, paddingBottom: 0 }}>
					<NewTask />
					<TodoListContainer />
				</Paper>
				<Footer />
			</Container>
		</React.Fragment>
	);
};

export default App;
