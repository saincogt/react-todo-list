/* eslint-disable react/prop-types */
import React from 'react';
import TodoItem from './TodoItem';
import { List, ListItem } from '@material-ui/core';

class TodoList extends React.Component {
	state ={
		tasks: this.props.tasks,
		visibleOption: this.props.visibleOption
	};

	renderList = () => {
		if (!this.props.tasks) {
			return <ListItem>Loading...</ListItem>;
		}
		var tasks = this.props.tasks;
		if (this.props.visibleOption === 'SHOW_REMAINING') {
			tasks = tasks.filter(item => item.completed === false);
		} else if (this.props.visibleOption === 'SHOW_COMPLETED') {
			tasks = tasks.filter(item => item.completed === true);
		}
		return tasks.map(item => <TodoItem key={item.id} id={item.id} text={item.text} completed={item.completed}/>);
	}

	render () {
		return (
			<List>
				{this.renderList()}
			</List>
		);
	}
}

export default TodoList;
