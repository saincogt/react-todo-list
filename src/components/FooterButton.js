/* eslint-disable react/prop-types */
import React from 'react';
import { Button } from '@material-ui/core';

const FooterButton = (props) => {
	return <Button style={{ flex: 1 }} onClick={props.handleClick}>{props.btnName}</Button>;
};

export default FooterButton;
