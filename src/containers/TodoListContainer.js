/* eslint-disable react/prop-types */
import React from 'react';
import { connect } from 'react-redux';
import TodoList from '../components/TodoList';

class TodoListContainer extends React.Component {
	render () {
		return (
			<TodoList tasks={this.props.tasks} visibleOption={this.props.visibleOption}/>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		tasks: state.tasks,
		visibleOption: state.visibleOption
	};
};

export default connect(mapStateToProps)(TodoListContainer);
